#!/usr/bin/env python
# coding=latin-1

import sys
import json
import requests
import requests.packages.urllib3
import logging
import urllib
import unicodedata

from fredrika import locationfinder, b64html2text
logger = logging.getLogger(__name__)

# disable urllib3 warnings and log mumbojumbo
requests.packages.urllib3.disable_warnings()
logging.getLogger("requests").setLevel(logging.INFO)

name = 'VMA'
verify_ssl = False

def byteify(input):
    if isinstance(input, dict):
        return {byteify(key): byteify(value)
                for key, value in input.iteritems()}
    elif isinstance(input, list):
        return [byteify(element) for element in input]
    elif isinstance(input, unicode):
        return input.encode('utf-8')
    else:
        return input
            
def parse_feed(db, Message, topic_id, app):
    logging.debug('Inside parser')

    # setup some defaults from the config
    archive_path = app.config['ARCHIVE_PATH']
    base_url = app.config['VMA_BASE_URL']
    changed = False

    # see what we have active
    logging.debug('Get active json')
    vmas = Message.query.filter(Message.active).all()
    # make a list of all active ones (we might need to disable them if they are inactive)
    active_vmas = []
    for row in vmas:
        identifier = row.raw['identifier']
        active_vmas.append(identifier)

    # do the query, get the index and see if we have any active
    url = base_url + '/index.json'
    try:
        r = requests.get(url, verify=verify_ssl)
        # force UTF-8 since VMA's often contain mixed encoding
        r.encoding = 'UTF-8'
    except Exception as e:
        logging.debug("Problem trying URL in %s %s" % (name, e))
        raise ValueError("Couldn't connect to %s url, bailing!" % name)

    data = r.json()
    alerts = []
    logging.debug("Parsing index result")
    logging.debug("Counting feed: %s" % data['count'])
    if data['count'] > 0:
        logging.debug("New %s(s): %s" % (name, data['count']))
        for alert in data['alert']:
            alerts.append(alert['identifier'])

    logging.debug('Iterater over alerts')
    for alert in alerts:
        # have we seen it before?
        if alert in [row.raw['identifier'] for row in vmas]:
            logging.debug("This VMA (%s) is already know, returning." % alert)
            return False
        else:
            url = base_url + '/%s.json' % alert
            logging.debug("Parsing %s: %s" % (name, url))
            r = requests.get(url, verify=verify_ssl)
            r.encoding = 'UTF-8'  # force UTF-8 response again

            try:
                data = r.json()
            except Exception as e:
                # wrong encoding or something
                logging.error("%s with id %s failed, exiting!" % (name, alert))
                raise ValueError('%s Couldnt parse the %s, bailing' % (name, alert))

            # lets download to archive file
            archive_file = archive_path + '/%s.json' % alert
            with open(archive_file, 'wb') as handle:
                r = requests.get(url, verify=verify_ssl,stream=True)
                r.encoding = 'UTF-8'
                for chunk in r.iter_content(chunk_size=512):
                    if chunk:  # filter out keep-alive new chunks
                        handle.write(chunk)
            
            # sometimes VMA testing tests takes place, lets ignore those.
            desc = data['info'][0]['description']
            if 'TEST' in desc or 'Meddelandet ska INTE publiceras' in desc:
                logging.info('%s Test message, skipping %s' % (name, alert))
                continue
            
            logging.info("Adding new %s with id %s" % (name, alert))
            
            # and sometimes they test stuff without location
            if 'area' in data['info'][0]:
                location = data['info'][0]['area'][0]['areaDesc'] 
            else:
                location = False

            # Override misspelled county :/
            if location:
                location = location.replace('Gataland', 'Gotaland')
            
            # current location
            logging.info("Location: %s", location)

            # lookup location from OpenStreetMap (OSM)
            try:
                location = locationfinder( location )
                logging.info("The location is probably %s" % location)
            except Exception as e:
                # wrong encoding or something
                logging.error("%s location conversion with id %s failed!" % (name, alert))

            message = data['info'][0]['resource'][0]['derefUri']
            text = b64html2text(message)
                
            msg = Message()
            msg.subject = 'VMA - Viktigt Meddelande till Allmänheten'
            msg.message = message
            msg.active = True
            msg.topic_id = topic_id
            msg.location = location
            msg.raw = data
            msg.text = text

            db.session.add(msg)
            db.session.commit()

            changed = True
            logging.info("%s with id %s, added successfully." % (name, alert))

    # update non-active alerts to be just that
    for vma in vmas:
        if vma.raw['identifier'] not in alerts:
            # ignore our manually sent tests
            if vma.raw['identifier'].startswith('HF-'):
                continue
            else:
                vma.active = False
                db.session.commit()
                logging.info("Deactivating inactive %s with id %s" % (name, vma.raw['identifier']))

    # last thing here
    return changed

