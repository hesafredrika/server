#!/usr/bin/env python
# -*- coding: utf-8 -*-

from flask.ext.script import Manager, Server
from flask.ext.script.commands import ShowUrls, Clean

from flask.ext.migrate import Migrate, MigrateCommand

from fredrika import app #create_app
from fredrika.models import db, User, Org


manager = Manager(app)
manager.add_command("server", Server())
manager.add_command("show-urls", ShowUrls())
manager.add_command("clean", Clean())
manager.add_command('db', MigrateCommand)

migrate = Migrate(app, db)

@manager.shell
def make_shell_context():
    """ Creates a python REPL with several default imports
        in the context of the app
    """
    return dict(app=app, db=db, User=User)


@manager.command
def create_db():
    """ Creates a database with all of the tables defined in
        your SQLAlchemy models
    """
    db.create_all()

@manager.command
def create_admin():
    """Creates the admin user."""
    org = Org.query.filter(Org.id==1).first()
    if not org:
        org = Org(id='1', name='Hesa Fredrika AB', description='')
        db.session.add(org)
        db.session.commit()
        db.session.refresh(org)

    admin = User(email="ny@hesafredrika.se", name='Fredrik', password="apanap", org_id=org.id)
    admin.admin = True
    db.session.add(admin)
    db.session.commit()

if __name__ == "__main__":
    manager.run()
