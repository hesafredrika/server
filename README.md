# Hesa Fredrika

This is the platform behind [hesafredrika.se](https://hesafredrika.se), which is driven by the Python framwork Flask.

The Python-Flask part was to some degree based on [Flask Foundation](https://travis-ci.org/JackStouffer/Flask-Foundation), but also on previous flask projects done by [Fredrik Lundhag](https://github.com/joltcan).

## Dependencies (i.e. what we use)
* Python
* Virtualenv
* Python-Flask and a myriad of libs, see further on
* Nginx
* Postgresql
* SSL certificate (i.e. [letsencrypt](https://letsencrypt.org), or [CloudFlare](https://cloudflare.com)).
* SQLalchemy
* A message broker (we use rabbitmq)
* celery for processing
* Google Firebase for push notifications
* Something to run python (we use uwsgi)

## Installation

* Clone this repo (git clone https://gitlab.com/hesafredrika/server)
* Install system packages (``` apt install uwsgi-python nginx postgresql python-setuptools ```)
* Configure your database, it has to be set to utf-8: ( In /etc/postgresql/9.N/main/postgresql.conf, set client_encoding='utf8' and reload it with ```service postgresql reload```).
* Copy nginx config to /etc/nginx/sites-available and enable (```ln -s /etc/nginx/sites-available/hesafredrika_se.conf /etc/nginx/sites-enabled```), verify the config, then reload nginx (nginx -t && service nginx reload).
* Install virtualenv :
  * ```virtualenv env --no-site-packages ```
  * ```source env/bin/activate ```
* Install required packages
  * ```pip install -r <requirements.txt```

### Create database
```sh
python manage.py create_db
python manage.py db init
python manage.py db migrate
python manage.py create_admin
```

### add admin user
This should really be a script, but for now, do it by running ```python``` and copy paste from below:
```python
from fredrika import db, models
user = models.User(email='someuser@example.com', password='<very good password>', orgid=1)
db.session.add(user)
db.session.commit()
```

### Run the system

* Start uwsgi and steer your browser to your URL. 
* For development you can also use the ```./run.sh``` script which is provided in the root.

## Translation

We were looking at doing proper translations, but this work has not been completed as of now. If you are interested, here is what you have to do to get started:
```sh
pybabel extract -F babel.cfg -o messages.pot fredrika/
pybabel update -i messages.pot -d fredrika/translations
edit fredrika/translations/sv/LC_MESSAGES/messages.po
pybabel compile -d fredrika/translations
```

## Misc information
### DB updated
If you modify the DB (models.py), then you can run this to make alembic do the migrations for you:
```sh
python manage.py db migrate
python manage.py db upgrade
```

### New python libraries
If you install some new python libraries, then you will need to "freeze" your installation to save them to requirements file:
```sh
pip freeze -l > requirements.txt
```

### TODO list
* [ ] Add organisation support in admin interface
* [ ] Add translations to the site
* [ ] Add more datasources and workers for those
* [ ] Make the logging and message edit work, and add proper HTML formatting.


