# -*- coding: utf-8 -*-
# This is the python config.py file for hesafredrika.se.

import os
basedir = os.path.abspath(os.path.dirname(__file__))

class BaseConfig(object):
    ASSETS_DEBUG = False
    SECRET_KEY = '<long and secret>'
    BABEL_DEFAULT_LOCALE = 'sv'
    ACCEPT_LANGUAGES = ['sv', 'en']
    CELERY_BROKER_URL = 'amqp://<user>:<password>@localhost:5672/<database>/'
    CELERY_RESULT_BACKEND = 'amqp://<user>:<password@localhost:5672/<database/'
    CELERY_TASK_SERIALIZER='json'
    CELERY_ACCEPT_CONTENT=['json']
    CACHE_NO_NULL_WARNING = True
    CACHE_TYPE = 'null'
    DEBUG = False
    DEBUG_TB_ENABLED = False
    FIREBASE_API_KEY = '<Firebase API key>' # Project > settings > cloud messaging
    FIREBASE_DRY_RUN = False
    PUSHBULLET_API_KEY = False 
    STATUSCAKE_MONITOR_URL = False
    LOCATIONIQ_API_KEY = False # register at https://locationiq.org
    MAIL_DEFAULT_SENDER = 'tut-noreply@hesafredrika.se'
    MAIL_SERVER = ''
    MAIL_USERNAME = ''
    MAIL_PASSWORD = ''
    MAIL_USE_TLS = True
    CAPTCHA_ENABLE = True
    CAPTCHA_NUMERIC_DIGITS = 5
    SESSION_TYPE = 'sqlalchemy'

class ProductionConfig(BaseConfig):
    SQLALCHEMY_DATABASE_URI = 'postgresql://postgresq://<user>:<password>@localhost/<database>'
    # first, make sure you enable cache in the code as well

class DevelopmentConfig(BaseConfig):
    ASSETS_DEBUG = True
    DEBUG = True
    #DEBUG_TB_INTERCEPT_REDIRECTS = False
    #DEBUG_TB_ENABLED = True
    SQLALCHEMY_DATABASE_URI = 'postgresql://postgresq://<user>:<password>@localhost/<database>'
    FIREBASE_DRY_RUN = True

