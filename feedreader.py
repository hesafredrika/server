#!/usr/bin/env python
# -*- coding: utf-8 -*-

from feedparsers import vma
from fredrika import db, app, notify_admin
from fredrika.models import Message, Topic
import requests
import logging
import logging.handlers
import sys
    
reload(sys)  # Reload does the trick!
sys.setdefaultencoding('UTF8')

logger = logging.getLogger()
if app.config['DEBUG']:
    logger.setLevel(logging.DEBUG)
else:
    logger.setLevel(logging.INFO)
handler = logging.handlers.SysLogHandler(address = '/dev/log', facility='syslog')
formatter = logging.Formatter( 'feeder[%(process)d]: %(levelname)s: %(message)s')
handler.setFormatter(formatter)
logger.addHandler(handler)

# disable urllib3 warnings and log mumbojumbo
requests.packages.urllib3.disable_warnings()
logging.getLogger("requests").setLevel(logging.WARNING)

def keepalive():
    # if not configured, return
    if not app.config['STATUSCAKE_MONITOR_URL']:
        return False
    url = app.config['STATUSCAKE_MONITOR_URL']
    try:
        r = requests.get(url)
    except:
        logger.error('Could not connect to keepalive check: %s', r.text)
    logging.debug('sending keepalive')

if __name__ == "__main__":
    logging.debug('Starting up parser')

    topic = Topic.query.filter_by(name='VMA').first()
    try:
        vma = vma.parse_feed(db, Message, topic.id, app)
    except Exception as e:
        logger.error('Could not run VMA check: %s' % e)
        raise ValueError('Error with VMA test, bailing')

    # we didn't break, so send keepalive to statuscake.
    keepalive()

    if vma:
        notify_admin('New VMA!')
