from flask import current_app

from fredrika import celery

@celery.task
def send_async_msg(to, msg):
    return "%s sent" % msg


def send_msg(to, msg, **kwargs):
    app = current_app._get_current_object()
    #msg = Message(app.config['FLASKY_MAIL_SUBJECT_PREFIX'] + ' ' + subject,
    #              sender=app.config['FLASKY_MAIL_SENDER'], recipients=[to])
    #msg.body = render_template(template + '.txt', **kwargs)
    #msg.html = render_template(template + '.html', **kwargs)
    send_async_msg.delay(to, msg)
