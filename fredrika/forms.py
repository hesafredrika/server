# -*- coding: utf-8 -*-


from flask_wtf import Form
from wtforms import BooleanField, TextField, PasswordField, SelectField, TextAreaField, SelectMultipleField
from wtforms.validators import DataRequired, Email, Length, EqualTo, NumberRange, ValidationError, Required
from wtforms.widgets import TextArea

from .models import User, Topic

from flask.ext.babel import lazy_gettext as _

class UserForm(Form):
    email = TextField(_("Email"), [Required()])
    name = TextField(_("Name") )
    password = PasswordField('Password')
    confirm = PasswordField('Repeat Password')

    def validate(self):
        check_validate = super(UserForm, self).validate()

        # if our validators do not pass
        if not check_validate:
            return False

        if self.password.data and self.confirm.data:
            if len(self.password.data) < 4:
                self.password.errors.append(_(u'Password should be longer then 8 characters'))
                return False
            if self.confirm.data != self.password.data:
                self.password.errors.append(_(u'Password does not match'))
                return False
        return True

class LoginForm(Form):
    email = TextField(_("Email"), [Required()])
    password = PasswordField(_("Password"), [DataRequired()])

    def validate(self):
        check_validate = super(LoginForm, self).validate()

        # if our validators do not pass
        if not check_validate:
            return False

        # does our user exist
        user = User.query.filter_by(email=self.email.data).first()
        if not user:
            self.email.errors.append(_(u'Wrong email or password'))
            return False

        # Do the passwords match
        if not user.check_password(self.password.data):
            self.email.errors.append(_(u'Wrong email or password'))
            return False

        return True

class MessageForm(Form):
    active = BooleanField(_('Active'))
    description = TextField(_('Description'))
    message = TextAreaField(_('Message'), [DataRequired()])
    subject = TextField(_('Subject'))
    location = TextField(_('Location'))
    url = TextField(_('URL'))
    topic_id = SelectField(_('Topic'), coerce=int )
    error = BooleanField(_('Error'))

    def validate(self):
        check_validate = super(MessageForm, self).validate()

        # if our validators do not pass
        if not check_validate:
            return False

        # Does our topic exist
        if not self.topic_id.data:
            self.topic_id.errors.append(_(u'Select topic'))
            return False

        return True

class MessageEditForm(Form):
    active = BooleanField(_('Active'))
    delivered = BooleanField(_('Active'))
    description = TextField(_('Description'))
    message = TextAreaField(_('Message'), [DataRequired()])
    subject = TextField(_('Subject'))
    location = TextField(_('Location'))
    url = TextField(_('URL'))
    error = BooleanField(_('Error'))

    def validate(self):
        check_validate = super(MessageEditForm, self).validate()
        if not check_validate:
            return False

        return True

class TopicForm(Form):
    name = TextField(_('Name'), [DataRequired()])
    note = TextAreaField(_('Note'))

    def validate(self):
        check_validate = super(TopicForm, self).validate()

        # if our validators do not pass
        if not check_validate:
            return False

        return True

class VolunteerForm(Form):
    name = TextField(_('Name'), [DataRequired()])
    note = TextAreaField(_('Note'), [DataRequired()])

    def validate(self):
        check_validate = super(VolunteerForm, self).validate()

        # if our validators do not pass
        if not check_validate:
            return False

        return True

class SignupForm(Form):
    email = TextField(_("E-mail"), [Required()])

    def validate(self):
        check_validate = super(SignupForm, self).validate()

        # if our validators do not pass
        if not check_validate:
            return False
        else:
            return True
