#! ../env/bin/python
# -*- coding: utf-8 -*-

from flask import Flask, request
from flask.ext.babel import Babel
from flask.ext.sqlalchemy import SQLAlchemy
from flask.ext.debugtoolbar import DebugToolbarExtension
from flask.ext.login import LoginManager
from flask.ext.bcrypt import Bcrypt
from flask.ext.cache import Cache
from pyfcm import FCMNotification
import requests
import urllib
import os

# for parsing the content to text
import base64
from bs4 import BeautifulSoup
from lxml import html

# add logging
import logging
from logging.handlers import SysLogHandler

# time to config
app = Flask(__name__)
app.config.from_object(os.getenv('APP_SETTINGS', 'config.DevelopmentConfig'))

# setup logging handler
handler = SysLogHandler(address='/dev/log')
handler.setLevel(logging.INFO)
formatter = logging.Formatter( 'HesaFredrikaServer [%(process)d]: %(levelname)s: %(message)s')
handler.setFormatter(formatter)
app.logger.addHandler(handler)

# session captcha
# captcha anyone?
from flask_session import Session
from flask_session_captcha import FlaskSessionCaptcha
import uuid

Session(app)


from flask_mail import Mail
from flask_mail import Message
mail = Mail()
mail.init_app(app)

def send_to_email(topic, subject, message, location, message_id, raw, users):
    with mail.connect() as conn:
	for user in users:
            message = raw['info'][0]['description']
	    message = message + '\n\n' + u'URL: https://hesafredrika.se/message/%d' % message_id
	    subject = subject
	    msg = Message(recipients=[user.email],
			  body=message,
                          html=message,
			  subject=subject)

	    conn.send(msg)

def send_to_topic(topic, subject, message, location, message_id, raw):
    """Background task to send to firebase ip"""
    with app.app_context():
        result = {}
        push_service = FCMNotification(api_key=app.config['FIREBASE_API_KEY'])
        data_message = {
                'location' : location[0]['boundingbox'],
                'location_name' : location[0]['display_name'],
                'lat' : location[0]['lat'], 
                'lon' : location[0]['lon'] ,
                'place_id' : location[0]['place_id'],
                'message' : message,
                'message_id' : message_id,
                'message_subject' : subject,
                'topic_name' : topic,
                'description' : raw['info'][0]['description'],
            } 
        app.logger.info(data_message)

        result = push_service.notify_topic_subscribers(
                topic_name = "%d" % topic,
                data_message = data_message, 
                dry_run = app.config['FIREBASE_DRY_RUN'],
                content_available = True,
            )

        app.logger.info("firebase: %s" % result)

        if app.config['FIREBASE_DRY_RUN']:
            notify_admin('Sending message to hosts %s' % subject)

        # TODO: here we should have some potential error handling/logging
        # Just remember the call is ansyncronus, so we dont know when it returns
        if result['failure'] != 0:
            msg = "FCM did not succeed with send %s to %s, notifying admin " % (subject, topic)
            notify_admin(msg)

        return result['results']

def notify_admin(message):
    with app.app_context():
        url = 'https://api.pushbullet.com/v2/pushes'
        headers = {'Access-Token' : app.config['PUSHBULLET_API_KEY']}
        payload = {'type' : 'note',
                'title' : 'Hesa Fredrika',
                'body' : message }
        try:
            r = requests.post(url, headers=headers, json=payload)
        except:
            return False

        return r.json()

def locationfinder(location):
    ''' takes a list of location names and tries to find them in openstreetmap '''
    # base_url = 'http://nominatim.openstreetmap.org/search?format=json&limit=1&countrycodes=se&'
    base_url = 'http://locationiq.org/v1/search.php?key=' + app.config['LOCATIONIQ_API_KEY'] + '&format=json&'
    with app.app_context():
        # probably only for tests:
        if 'hela landet' in location:
            location = 'Sverige'
       
        url = base_url + urllib.urlencode( {"q":location} )

        try:
            r = requests.get(url)
        except Exception as error:
            msg = 'error doing query to openstreetmap novatim: %s' % error
            notify_admin(msg)
            return False

        if r.json():
            return r.json()
        else:
            return False
        

def b64html2text(html):
    try:
        html = base64.b64decode(html)
    except:
        html = False

    if html:
        soup = BeautifulSoup(html, "html.parser")
        text = soup.text
    else:
        text = False

    return text

####################
#### extensions ####
####################

login_manager = LoginManager()
login_manager.init_app(app)
bcrypt = Bcrypt(app)
toolbar = DebugToolbarExtension(app)
cache = Cache(app)
db = SQLAlchemy(app)

captcha = FlaskSessionCaptcha(app)

babel = Babel(app)
@babel.localeselector
def get_locale():
    accept_languages = app.config.get('ACCEPT_LANGUAGES')
    return request.accept_languages.best_match(accept_languages)

###################
#### flask-login ####
####################

from models import User, Logging
login_manager.login_view = "main.login"
login_manager.login_message_category = "warning"

@login_manager.user_loader
def load_user(user_id):
        return User.query.filter(User.id == int(user_id)).first()

login_manager.init_app(app)

# register our blueprints
from fredrika.main.views import main
app.register_blueprint(main)

