��    0      �                        &     -     <     M     Y     _  %   e     �               !     *     F     K     S     `     l  	   q     {     �     �  +   �     �     �     �                 #     c   A     �     �     �  I   �               "     &     .  
   4     ?     W     i  +   |  -   �     �  �  �  	   �     �     �     �     �     �     �  .   �     		     �	     �	     �	     �	     �	  
   �	     �	     �	     �	     �	     �	  	   
  "   
  ,   5
  &   b
     �
     �
     �
     �
     �
  %   �
  c   �
     J     R     f  P   o     �     �     �  
   �     �     �     �          &  *   6     a     �    edited. Active Active warning Deleted Message  Description Email Error Follow this link to see all messages: Få viktiga meddelanden till allmänheten, vädervarningar och annan kritisk samhällsinformation direkt till din mobiltelefon. Inactive warning Link Location Location could not be found Logs Message Message list Message log Name Next page Note Password Password does not match Password should be longer then 8 characters Permanent link to this message Previous page Select topic Send Sign in Sign out Sorry, This message does not exist. Ställ upp som frivillig när hjälporganisationer i din närhet behöver hjälp i en krissituation Subject The message with id  Tillbaka To send a message, enter your text and select the topic, then press send. Topic Topics URL Updated Users Volunteers Wrong email or password You are logged in You are logged out You can only see this if you are logged in! Your message was sent added to the send queue could not be found Project-Id-Version: PROJECT VERSION
Report-Msgid-Bugs-To: code@hesafredrika.se
POT-Creation-Date: 2017-12-17 17:26+0100
PO-Revision-Date: 2015-11-18 22:48+0100
Last-Translator: Fredrik Lundhag <fredrik@hesafredrika.se>
Language: sv
Language-Team: sv <LL@li.org>
Plural-Forms: nplurals=2; plural=(n != 1)
MIME-Version: 1.0
Content-Type: text/plain; charset=utf-8
Content-Transfer-Encoding: 8bit
Generated-By: Babel 2.3.4
  ändrat. Aktiv Aktiv varning Tog bort meddelande  Description E-post Error Följ denna länk för att se alla meddelande: Få viktiga meddelanden till allmänheten, vädervarningar och annan kritisk samhällsinformation direkt till din mobiltelefon. Inaktiv varning Länk Plats Platsen kunde inte hittas Loggar Meddelande Meddelandelistan Meddelanden Namn Nästa sida Notering Lösenord Lösenorden stämmer inte överens Lösenordet behlver vara 8 tecken eller fler Permanent länk till detta meddelandet Föregående sida Välj ämne Skicka Logga in Logga ut Tyvärr, meddelandet kan inte hittas. Ställ upp som frivillig när hjälporganisationer i din närhet behöver hjälp i en krissituation Ärende Meddelandet med id  Tillbaka För att skicka ett meddelande, skriv in text, välj topic och klicka på Skicka Ämne Ämnen URL Uppdaterad Administratörer Organisationer Fel email eller lösenord Du är inloggad Du är utloggad Du kan bara se detta när du är inloggad! Ditt meddelande är lagt i kön kan inte hittas 