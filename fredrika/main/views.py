# -*- coding: utf-8 -*-

from flask import Blueprint, render_template, flash, request, redirect, url_for, g, Flask, Response, jsonify
from flask.ext.login import login_user, logout_user, login_required, current_user
from sqlalchemy.orm import load_only

from fredrika.forms import LoginForm, MessageForm, UserForm, TopicForm, MessageEditForm, VolunteerForm, SignupForm
from fredrika.models import User, Org, Municipality, Topic, Logging, Volunteer, Message, Signup
from fredrika import db, locationfinder, notify_admin, send_to_topic, send_to_email, b64html2text, captcha
from flask.ext.babel import gettext
import requests
import json
import base64
import datetime
import time

main = Blueprint('main', __name__)

@main.route('/', methods=["GET", "POST"])
#@cache.cached(timeout=1000)
def home():
    if current_user.is_authenticated():
        return redirect(url_for('.dashboard'))

    form = SignupForm()

    if form.validate_on_submit():
        if captcha.validate():
            pass
        else:
            flash( gettext(u'Felaktig Captcha'), 'error')
            return redirect(url_for('.home'))

        # time to validate email
        email = Signup.query.filter(Signup.email==form.email.data).first()
        if not email:
            signup = Signup()
            form.populate_obj(signup)
            db.session.add(signup)
            db.session.commit()
            flash( gettext(u'Din e-mail finns nu på VMA-utskickslistan: ') + signup.email, 'success')
            return redirect(url_for('.home'))
        else:
            flash( gettext(u'Din e-mail finns redan i VMA-utskickslistan! '), 'error')
            return redirect(url_for('.home'))

    return render_template('index.html', form=form)

#    return render_template('index.html')

@main.route('/om')
def about():

    return render_template('about.html')

@main.route('/send', methods=["GET", "POST"])
@login_required
def send():
    form = MessageForm()

    topics = []
    td = Topic.query.filter(Topic.org_id==current_user.org_id).order_by(Topic.name).all()
    for t in td:
        topics.append((t.id, t.name))

    form.topic_id.choices = topics

    if form.validate_on_submit():
        #for the loop to work
        org_id=current_user.org_id
        user_id=current_user.id

        # send mesg, iterate through form.topic.data
        sent = []

        # these two are not used for manual sends
        location = False # feel free to extend with osm location data
        source_id = "manual" 

        topic = int(form.topic_id.data)
        sent.append(topic)
        message = form.message.data.encode('utf-8').strip()
        location = form.location.data.encode('utf-8').strip()

        raw = {}
        raw['identifier'] = datetime.datetime.utcfromtimestamp(time.time()).strftime('HF-%Y%m%d-%H%M%S-Z')
        raw['info'] = [{ 'description' : form.description.data } ]

        msg = Message()
        msg.subject = form.subject.data
        msg.message = base64.b64encode(message)
        msg.active = True
        msg.raw = raw
        msg.topic_id = topic
        msg.location = locationfinder(location)
        if not msg.location:
            form.location.errors.append(gettext('Location could not be found'))
            return render_template('dashboard.html', form=form)

        db.session.add(msg)
        db.session.commit()

        # TODO: Refactor this to use Message table instead

        # we do this to clean the form by reloading the page
        flash(gettext('Your message was sent added to the send queue'),'success')
        return redirect(url_for('.dashboard'))

    return render_template('dashboard.html', form=form)

@main.route('/topics', methods=['GET', 'POST'])
@login_required
def topics():
    topics = Topic.query.order_by(Topic.name).all()
    form = TopicForm()
    if form.validate_on_submit():
        topic = Topic.query.filter(Topic.name==form.name.data).first()
        if not topic:
            topic = Topic()
            form.populate_obj(topic)
            db.session.add(topic)
            db.session.commit()
            flash(u'Topic %s added.' % form.name.data )
            return redirect(url_for('.topics'))
        else:
            form.name.errors.append(u'Topic already exists')

    return render_template('topics.html', data=topics, form=form)

@main.route('/topic/edit/<int:topic>', methods=['GET', 'POST'])
@login_required
def topic_edit(topic=False):
    topic = Topic.query.filter(Topic.id==topic).first()
    form = TopicForm(obj=topic)
    if form.validate_on_submit():
        form.populate_obj(topic)
        db.session.add(topic)
        db.session.commit()
        flash(u'Topic %s edited.' % topic.name )
        return redirect(url_for('.topics'))

    return render_template('topics.html', data=False, form=form)

@main.route('/dashboard', methods=['GET', 'POST'])
@main.route('/dashboard/<int:page>', methods=['GET', 'POST'])
@login_required
def dashboard(page=1):

    # handle toggle of active to inactive
    inactive = request.args.get("inactivate")
    if inactive:
        msg = Message.query.filter_by(id=int(inactive)).first()
        msg.active = False
        db.session.add(msg)
        db.session.commit()
        return redirect(url_for('.dashboard'))

    active = request.args.get("activate")
    if active:
        msg = Message.query.filter_by(id=int(active)).first()
        msg.active = True
        db.session.add(msg)
        db.session.commit()
        return redirect(url_for('.dashboard'))

    # post per page for pagination
    ppp = 50
    query = Message.query.filter(Message.org_id==current_user.org_id).order_by(db.desc(Message.created)).paginate(page, ppp, False)
    return render_template('logging.html', data=query)

@main.route('/users', methods=['GET', 'POST'])
@login_required
def users():
    users = User.query.filter(User.org_id==current_user.org_id).order_by(User.created).all()
    form = UserForm()
    if form.validate_on_submit():
        user = User.query.filter(User.email == form.data['email']).first()
        if user:
            form.email.errors.append(u'Topic already exists')
        else:
            user = User(email=form.data['email'],
                           name=form.data['name'],
                           password=form.data['password'],
                           org_id = current_user.org_id,
                           )
            user.admin = True
            db.session.add(user)
            db.session.commit()
            flash(u'User created')
            return redirect(url_for('.users'))

    return render_template('users.html', form=form, data=users)

@main.route('/users/edit/<int:edit>', methods=['GET', 'POST'])
def user_edit(edit=0):
    user = User.query.filter(User.id==edit, User.org_id==current_user.org_id).first()

    form = UserForm(obj=user)

    if form.validate_on_submit():
        user.name = form.data['name']
        user.email= form.data['email']
        if form.data['password']:
            user.set_password(form.data['password'])

        db.session.commit()
        
        flash('User updated')
        return redirect(url_for('.users'))
    
    return render_template('users.html', form=form, data=False)

@main.route('/users/delete/<int:delete>', methods=['GET', 'POST'])
def user_delete(delete=0):
    user = User.query.filter(User.id==delete, User.org_id==current_user.org_id).delete()
    db.session.commit()
    flash(u'User deleted')
    return redirect(url_for('.users'))

@main.route('/signups')
@login_required
def signups():
    delete = request.args.get('delete')
    if delete:
        Signup.query.filter_by(id=int(delete)).delete()
        db.session.commit()
        flash(gettext("Deleted email ") +  str(delete), "success")
        return redirect(url_for(".signups"))

    users = Signup.query.order_by(Signup.created).all()
    return render_template('signups.html', data=users)


''' Volunteer stuff '''
@main.route('/volunteer', methods=['GET', 'POST'])
@login_required
def volunteer():
    volunteers = Volunteer.query.order_by(Volunteer.name).all()
    form = VolunteerForm()
    if form.validate_on_submit():
        volunteer = Volunteer.query.filter(Volunteer.name==form.name.data).first()
        if not volunteer:
            volunteer = Volunteer()
            form.populate_obj(volunteer)
            db.session.add(volunteer)
            db.session.commit()
            flash(u'Volunteer topic%s added.' % form.name.data )
            return redirect(url_for('.volunteer'))
        else:
            form.name.errors.append(u'Volunteer topic already exists')

    return render_template('volunteers.html', data=volunteers, form=form)

@main.route('/volunteer/edit/<int:topic>', methods=['GET', 'POST'])
@login_required
def volunteer_edit(volunteer=False):
    volunteer = Volunteer.query.filter(Volunteer.id==volunteer).first()
    form = TopicForm(obj=volunteer)
    if form.validate_on_submit():
        form.populate_obj(volunteer)
        db.session.add(volunteer)
        db.session.commit()
        flash(u'Volunteer topic %s edited.' % volunteer.name )
        return redirect(url_for('.volunteer'))

    return render_template('volunteers.html', data=False, form=form)

''' End volunteer stuff '''

@main.route('/admin/org', methods=['GET', 'POST'])
@login_required
def admin_org(page=1):
    log = Org.query.all()

    return render_template('logging.html', data=log)

@main.route("/message/edit/<int:mid>", methods=['GET', 'POST'])
@login_required
def message_edit(mid=0):
    delete = request.args.get('delete')
    if delete:
        Message.query.filter_by(id=mid).delete()
        db.session.commit()
        flash(gettext("Deleted Message ") + str(mid), "success")
        return redirect(url_for('.dashboard'))

    message = Message.query.filter_by(id=mid).first()
    form = MessageEditForm(obj=message)
    if form.validate_on_submit():
        form.populate_obj(message)
        # Override the auto form to handle utf-8 and b64encode
        message.message = base64.b64encode(message.message.encode('utf-8'))
        message.location = locationfinder(message.location.encode('utf-8'))

        message.text = b64html2text(message.message)

        # Helluva ugly hack, but deadlines!
        raw = {}
        raw['identifier'] = message.raw['identifier'] 
        raw['info'] = [{ 'description' : form.description.data } ]
        message.raw = raw
        db.session.add(message)
        db.session.commit()
        flash(gettext("Message") + str(message.id) + gettext(" edited.") )
        return redirect(url_for('.dashboard'))

    return render_template("message_edit.html", data=message, form=form)

@main.route("/login", methods=["GET", "POST"])
def login():
    form = LoginForm()

    if form.validate_on_submit():
        user = User.query.filter_by(email=form.email.data).one()
        login_user(user)

        flash(gettext("You are logged in"), "success")
        #return redirect(request.args.get("next") or url_for(".dashboard"))
        return redirect(url_for(".dashboard"))

    return render_template("login.html", form=form)

@main.route("/logout")
def logout():
    logout_user()
    flash(gettext("You are logged out"), "success")

    return redirect(url_for(".home"))

@main.route("/restricted")
@login_required
def restricted():
    return gettext("You can only see this if you are logged in!"), 200

''' API start '''
@main.route("/api/<api_version>/<name>", methods=['GET', 'POST'])
@main.route("/api/<api_version>/<name>/<int:i_id>", methods=['GET', 'POST'])
def api(api_version=1.0, name=False, i_id=False):
    data = []
    query = False

    if 'volunteers' in name:
        if i_id:
            query = Volunteer.query.filter(Volunteer.id==i_id).all()
        else:
            query = Volunteer.query.all()
    if 'topics' in name:
        if i_id:
            query = Topic.query.filter(Topic.id==i_id).all()
        else:
            query = Topic.query.all()

    if query:
        for row in query:
            if row.updated: 
                updated = row.updated
            else: 
                updated = row.created

            data.append(  { 
                'name': row.name,
                'id': row.id,
                'created': row.created.strftime('%y%m%dT%H%M%S'),
                'updated': updated.strftime('%y%m%dT%H%M%S'),
                'note': row.note,
                'url': row.url,
                })

    if 'alarms' in name:
        if i_id:
            query = Message.query.filter(Message.id==i_id).all()
        else:
            query = Message.query.filter(Message.delivered==True).order_by(Message.id.desc()).limit(10)

        for row in query:
            # make sure our timestamps are nice
            if row.updated: 
                updated = row.updated
            else: 
                updated = row.created

            data.append ( { 
                'id': row.id,
                'active' : row.active,
                'created': row.created.strftime('%y%m%dT%H%M%S'),
                'updated': updated.strftime('%y%m%dT%H%M%S'),
                'location' : row.location,
                'message': row.message,
                'url': row.url,
                'subject' : row.subject,
                'text': row.text,
                'description' : row.raw['info'][0]['description'],
                })
           
    if len(data) is 0:
        data = [{'error' : 'Cannot find the requested endpoint'}]

    jsondata = json.dumps(data)
    response = Response(response=jsondata, status=200, mimetype='application/json')
    return response

''' API end '''

''' alarm function start'''
@main.route("/fix", methods=['GET'])
def fix():
    messages = Message.query.filter_by(location=None).all()
    for m in messages:
        loc = [m.raw['info'][0]['area'][0]['areaDesc']]
        location = locationfinder(loc)
        m.location = location
        print m.id, m.subject
        m.subject = m.raw['info'][0]['event']
        m.message = m.raw['info'][0]['resource'][0]['derefUri']
        db.session.add(m)
        db.session.commit()
    return "Processing Done"

@main.route("/sendalarm", methods=['GET'])
def sendalarm():
    e = False
    messages = Message.query.filter(Message.delivered == 'f', Message.location != None, Message.error == 'f').all()
    users = Signup.query.order_by(Signup.created).all()

    for m in messages:
        # Loop over messages, make sure that we could parse something from the location
        if m.location:
            if not 'boundingbox' in m.location[0]:
                try:
                    m.location = locationfinder(m.location.encode('utf-8'))
                except:
                    m.location = locationfinder(m.location)

            if m.raw['info'][0]['description']:
                text_message = m.raw['info'][0]['description']
            else:
                text_message = m.text
            try:
                send = send_to_email(m.topic.id, m.subject, text_message, m.location, m.id, m.raw, users)
            except requests.exceptions.RequestException as e:
                pass
        else:
            e = 'No location available'

        if not e:
            notify_admin('Delivered message %s!' % (m.id))
            m.delivered='t'
        else:
            notify_admin('Could not deliver message %s: %s' % (m.id, e))
            m.error='t'

        db.session.add(m)
        db.session.commit()

    return "0"


@main.route("/sendalarm_old", methods=['GET'])
def sendalarm_old():
    e = False
    messages = Message.query.filter(Message.delivered == 'f', Message.location != None, Message.error == 'f').all()
    
    for m in messages:
        # Loop over messages, make sure that we could parse something from the location
        if m.location:
            if not 'boundingbox' in m.location[0]:
                try:
                    m.location = locationfinder(m.location.encode('utf-8'))
                except:
                    m.location = locationfinder(m.location)

            if m.raw['info'][0]['description']:
                text_message = m.raw['info'][0]['description']
            else:
                text_message = m.text
            try:
                #send = send_to_topic(m.topic.id, m.subject, text_message, m.location, m.id, m.raw)
                send = send_to_topic(m.topic.id, m.subject, text_message, m.location, m.id, m.raw)
            except requests.exceptions.RequestException as e:
                pass
        else:
            e = 'No location available'

        if not e:
            notify_admin('Delivered message %s!' % (m.id))
            m.delivered='t'
        else:
            notify_admin('Could not deliver message %s: %s' % (m.id, e))
            m.error='t'

        db.session.add(m)
        db.session.commit()

    return "0"

''' alarm function end '''

''' public info pages '''
import base64
@main.app_template_filter()
def b64decode(data):
    decoded = base64.b64decode(data)
    try:
        data = decoded.decode('utf-8')
    except:
        data = decoded
    return data


@main.route('/message')
@main.route('/message/<int:mid>')
@main.route('/messages')
@main.route('/messages/page/<int:page>')
def message_public(mid=False, page=1):
    # posts per page
    ppp = 20

    if mid:
        message = Message.query.filter(Message.id==mid).paginate(page, ppp, False)
    else:
        message = Message.query.filter(Message.delivered == 't').order_by(Message.created.desc()).paginate(page, ppp, False)

    if not message:
        flash (gettext("The message with id ") + str(mid) + gettext("could not be found"), 'error')
        return redirect(url_for('.messages'))
    
    return render_template('message.html', data = message)

''' end public info pages '''

