# encoding: utf-8

from fredrika import db, bcrypt, cache
from sqlalchemy.sql import func
from flask.ext.sqlalchemy import SQLAlchemy
from flask.ext.login import UserMixin, AnonymousUserMixin
from werkzeug.security import generate_password_hash, check_password_hash
from sqlalchemy.dialects.postgresql import JSONB

# Define models
class Org(db.Model):
    id = db.Column(db.Integer(), primary_key=True)
    name = db.Column(db.String(80), unique=True)
    description = db.Column(db.String(255))
    logs = db.relationship('Logging', backref='Org')

    created = db.Column(db.DateTime(timezone=True), server_default=func.now())
    updated = db.Column(db.DateTime(timezone=True), onupdate=func.now())

class User(db.Model, UserMixin):
    __tablename__ = "users"
    id = db.Column(db.Integer(), primary_key=True)
    email = db.Column(db.String(255), unique=True)
    name = db.Column(db.String())
    password = db.Column(db.String())
    admin = db.Column(db.Boolean())
    active = db.Column(db.Boolean(), default=False)

    created = db.Column(db.DateTime(timezone=True), server_default=func.now())
    updated = db.Column(db.DateTime(timezone=True), onupdate=func.now())
    
    created_by = db.Column(db.Integer)

    org_id = db.Column(db.Integer, db.ForeignKey(Org.id))
    orgs = db.relationship(
            'Org',
            backref=db.backref('users',
                                uselist=True,
                                cascade='delete,all'))

    def __init__(self, email, name, password, org_id):
        self.email= email
        self.name = name
        self.set_password(password)
        self.org_id = org_id

    def set_password(self, password):
        self.password = generate_password_hash(password)

    def check_password(self, value):
        return check_password_hash(self.password, value)

    def is_authenticated(self):
        if isinstance(self, AnonymousUserMixin):
            return False
        else:
            return True

    def is_active(self):
        return True

    def is_admin(self):
        if self.admin =='t':
            return True
        else:
            return False

    def is_anonymous(self):
        if isinstance(self, AnonymousUserMixin):
            return True
        else:
            return False

    def get_id(self):
        return self.id

    def __repr__(self):
        return '<User %r>' % self.email

class Municipality(db.Model):
    id = db.Column(db.Integer(), primary_key=True)
    name = db.Column(db.String())

class Topic(db.Model):
    id = db.Column(db.Integer(), primary_key=True)
    name = db.Column(db.String())
    note = db.Column(db.String())
    priority = db.Column(db.Integer())
    url = db.Column(db.String())
    org_id = db.Column(db.Integer, db.ForeignKey('org.id'))

    created = db.Column(db.DateTime(timezone=True), server_default=func.now())
    updated = db.Column(db.DateTime(timezone=True), onupdate=func.now())

class Volunteer(db.Model):
    id = db.Column(db.Integer(), primary_key=True)
    name = db.Column(db.String())
    note = db.Column(db.String())
    priority = db.Column(db.Integer())
    url = db.Column(db.String())
    org_id = db.Column(db.Integer, db.ForeignKey('org.id'))

    created = db.Column(db.DateTime(timezone=True), server_default=func.now())
    updated = db.Column(db.DateTime(timezone=True), onupdate=func.now())

class Logging(db.Model):
    id = db.Column(db.Integer(), primary_key=True)

    org_id = db.Column(db.Integer, db.ForeignKey('org.id')) 
    message_id = db.Column(db.Integer, db.ForeignKey('message.id')) 
    topic_id = db.Column(db.Integer, db.ForeignKey('topic.id')) 
    user_id = db.Column(db.Integer, db.ForeignKey('users.id')) 

    org = db.relationship('Org', backref='Logging', lazy="joined" )
    message = db.relationship('Message', backref='Logging', lazy="joined" )
    topic = db.relationship('Topic', backref='Logging', lazy="joined" )
    user = db.relationship('User', backref='Logging', lazy="joined" )

    created = db.Column(db.DateTime(timezone=True), server_default=func.now())
    updated = db.Column(db.DateTime(timezone=True), onupdate=func.now())

class Message(db.Model):
    id = db.Column(db.Integer(), primary_key=True)

    topic_id = db.Column(db.Integer, db.ForeignKey('topic.id')) 
    topic = db.relationship('Topic', backref='Message', lazy="joined" )
    
    active = db.Column(db.Boolean(), default=False)
    delivered = db.Column(db.Boolean(), default=False)
    error = db.Column(db.Boolean(), default=False)
    location = db.Column(JSONB)
    message = db.Column(db.String())
    text = db.Column(db.String())
    raw = db.Column(JSONB)
    subject = db.Column(db.String())
    url = db.Column(db.String())

    created = db.Column(db.DateTime(timezone=True), server_default=func.now())
    updated = db.Column(db.DateTime(timezone=True), onupdate=func.now())
   
    # for logging purposes
    org_id = db.Column(db.Integer, db.ForeignKey('org.id'), default=1) 
    user_id = db.Column(db.Integer, db.ForeignKey('users.id'), default=1) 
    org = db.relationship('Org', backref='Message', lazy="joined" )
    user = db.relationship('User', backref='Message', lazy="joined" )

class Signup(db.Model):
    id = db.Column(db.Integer(), primary_key=True)
    name = db.Column(db.String(255), unique=True)
    email = db.Column(db.String(255), unique=True)
    
    created = db.Column(db.DateTime(timezone=True), server_default=func.now())
